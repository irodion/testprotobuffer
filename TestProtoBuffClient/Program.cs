﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuffLocal;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;



namespace TestProtoBuffClient
{
    /// <summary>
    /// Simple TCP client, connet to server and get data
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch sw1 = new Stopwatch();
            TcpClient connectTo = new TcpClient("127.0.0.1", 20100);


            NetworkStream stream = connectTo.GetStream();

            long received = 0;

            while (connectTo.Connected)
            {
                if (connectTo.Available > 0)
                {
                    sw1.Restart();
                    var readed = Serializer.DeserializeWithLengthPrefix<SimpleMessage>(stream,ProtoBuf.PrefixStyle.Base128);
                    sw1.Stop();

                    if (readed != null)
                    {
                        ++received;
                        Console.WriteLine("Readed: address: {0} dataSize: {1} all: {2} time: {3}", readed.Address.DeviceAdderss, readed.RawData.Length, received, sw1.ElapsedMilliseconds);
                    }
                }

            }

        }
    }
}
