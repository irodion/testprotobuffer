﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuffLocal;
using ProtoBuf;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Diagnostics;

namespace TestProtoBuffServer
{
    /// <summary>
    /// Simple TCP server, wait for connection and send "serialized" data
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
         
            Stopwatch sw1 = new Stopwatch();

            TcpListener listener = new TcpListener(IPAddress.Loopback, 20100);
            listener.Start();

            Console.WriteLine("Waiting for connection");
            while (true)
            {
                TcpClient client = listener.AcceptTcpClient();
                NetworkStream netStream = client.GetStream();

                sw1.Restart();
                int i = 0;
                for (i = 0; 10000 > i; ++i)
                {
                    SimpleMessage msg = ProtoBuffLocal.Program.createRandomMessage();
                    Serializer.SerializeWithLengthPrefix<SimpleMessage>(netStream, msg,ProtoBuf.PrefixStyle.Base128);
                    Console.WriteLine("Sended: {0} {1}", msg.Address.DeviceAdderss, msg.RawData.Length);
                }
                
                netStream.Flush();
                sw1.Stop();

                Console.WriteLine("Data sended, {0} for {1} ms", i, sw1.ElapsedMilliseconds);
                Console.ReadLine();
            }
            
            

        }
    }
}
