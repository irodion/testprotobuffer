﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;
using System.IO;
using System.Diagnostics;

namespace ProtoBuffLocal
{
    public class Program
    {


        public static Random randomProvider = new Random(DateTime.Now.Millisecond + DateTime.Now.Second);

        static void Main(string[] args)
        {

            Stopwatch sw1 = new Stopwatch();
            

            var rackLists = new List<SimpleMessage>
            {
                new SimpleMessage
                {
                    Type = MessageType.MessagePositioning,
                    Address = new DeviceAddress {BaseNumber = 0x00, DeviceAdderss = 0x00 },
                    RawData = new byte[] { 0x01, 0x02, 0x03, 0x04 }
                },

                new SimpleMessage
                {
                    Type = MessageType.MessageService,
                    Address = new DeviceAddress {BaseNumber = 0x01, DeviceAdderss = 0x34 },
                    RawData = new byte[] { 0x06, 0x07, 0x08, 0x04 }
                }
            };

            using (var myfile = File.Create("temp.bin"))
            {
                myfile.Position = 0;
                
                for (int i = 0; 10000 > i; ++i)
                {
                    SimpleMessage tempRack = createRandomMessage();
                    rackLists.Add(tempRack);
                }
                sw1.Restart();
                Serializer.Serialize(myfile, rackLists);
                sw1.Stop();
                myfile.Flush();
                myfile.Close();
                Console.WriteLine("Serialized in {0} ms", sw1.ElapsedMilliseconds);
            }
            using (var myfile = File.Open("temp.bin", FileMode.Open))
            {
                
                myfile.Position = 0;
                sw1.Restart();
                var readed = Serializer.Deserialize<List<SimpleMessage>>(myfile);
                sw1.Stop();
                Console.WriteLine("UnSerialized in {0} ms", sw1.ElapsedMilliseconds);
                myfile.Close();
            }

            Console.ReadLine();
        }

        public static SimpleMessage createRandomMessage()
        {
            SimpleMessage newMessage = new SimpleMessage();
            // it's wrong!!! random always the same in short terms

            // populate with random data
            newMessage.Type = MessageType.MessagePositioning;
            DeviceAddress currentAddress = new DeviceAddress();
            currentAddress.DeviceAdderss = randomProvider.Next(0xFFFF);
            newMessage.Address = currentAddress;
            
            int dataCounter = randomProvider.Next(0x19000);
            newMessage.RawData = new byte[dataCounter];

            for (int i = 0; i < dataCounter; ++i)
            {
                newMessage.RawData[i] = (byte) randomProvider.Next(0xFF);
            }

            return (newMessage);
        }

    }
}
