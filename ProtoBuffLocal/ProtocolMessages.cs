﻿using System;
using ProtoBuf;

namespace ProtoBuffLocal
{
    
    enum MessageType = { MessagePositioning = 0x00, MessageDiagnostic = 0x01, MessageCommand = 0x02, MessageError = 0x03}

    [ProtoContract]
    class MessageHeader
    {
        [ProtoMember(1)]
        int MessageSign = 0x55AA55AA;

        [ProtoMember(2)]
        MessageType messageType;

        public MessageHeader(int MessageSign, MessageType type)
        {
            this.MessageSign = MessageSign;
            this.messageType = type;
        }
        
        public MessageHeader()
        {

        }

    }

    [ProtoContract]
    class MessagePositioning
    {
        int address;

    }
}
