﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProtoBuf;

namespace ProtoBuffLocal
{
    public enum MessageType
    {
        MessagePositioning,
        MessageService,
        MessageVoice
    }

    [ProtoContract]
    public struct DeviceAddress
    {
        [ProtoMember(3)]
        public int BaseNumber {get; set;}

        [ProtoMember(4)]
        public int DeviceAdderss { get; set; }
    }

    [ProtoContract]
    public class SimpleMessage
    {
        [ProtoMember(1)]
        public MessageType Type {get; set;}

        [ProtoMember(2)]
        public DeviceAddress Address { get; set; }

        [ProtoMember(5)]
        public byte[]  RawData { get; set; }
    }
}
